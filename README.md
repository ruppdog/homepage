[![Build Status](http://jenkins.tylerruppert.com:8080/job/Homepage-Staging/badge/icon)](http://jenkins.tylerruppert.com:8080/job/Homepage-Staging/)

# Tyler Ruppert's Homepage

This is the public repo that runs my site, [tylerruppert.com](http://www.tylerruppert.com).