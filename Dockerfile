FROM ruppdog/hugo

ENV HUGO_BASEURL "http://tylerruppert.com/"
ENV HUGO_TITLE "Tyler Ruppert"
ENV HUGO_COPYRIGHT "(c) 2015 Tyler Ruppert."
ENV HUGO_NAME "Tyler Ruppert"
ENV HUGO_EMAIL "tylerruppert@gmail.com"
ENV HUGO_GITHUB "https://github.com/truppert"
ENV HUGO_BITBUCKET "https://bitbucket.org/ruppdog"
ENV HUGO_LINKEDIN "https://www.linkedin.com/in/tylerruppert"
ENV HUGO_TAGLINE "DevOps Engineer"
ENV HUGO_GOOGLEANALYTICS "UA-82479054-1"
ENV HUGO_HOME "Home"
ENV HUGO_POST "/:slug/:title"
ENV HUGO_CODE "/:slug/"

ADD site/ /app/site

COPY templates /app/templates

# Set working directory
WORKDIR /app/site
