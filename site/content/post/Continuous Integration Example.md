+++
date = "2016-08-14"
title = "Continuous Integration Example"
description = "Continuous Integration of this site using Bitbucket, Jenkins, and Docker Cloud"
slug = "post"
+++

Continuous Integration is the act of continuously merging code as it is ready.  This is different from Continuous Deployment in that the code doesn't automatically go out to production.

I learn by example, and I'm sure a lot of other people do to.  To that end, I set up simple Continuous Integration for this site using Bitbucket, Jenkins and Docker Cloud.

##### Here is the basic workflow:
* Every time I push code to the development branch in BitBucket, a webook is used to trigger a Jenkins build
* Jenkins builds the new docker image and pushes it to Dockerhub with a "staging" tag
* (Currently Manually) The service in Docker Cloud is redeployed and is now up to date

#### Bitbucket setup:
Repo: https://bitbucket.org/ruppdog/homepage

The only thing special about this repo is that there is a webook set up that hooks Jenkins every time there is a push:
```
Title: Staging Jenkins
URL: http://jenkins.tylerruppert.com:8080/bitbucket-hook/
Status: Active
Trigges: Repository Push
```

#### Stackfile in Docker Cloud
```
docker:
  image: 'docker:1.12-dind'
  privileged: true
jenkins:
  image: 'jenkins:alpine'
  ports:
    - '8080:8080'
  volumes:
    - /var/jenkins_home
homepage:
  image: 'ruppdog/homepage:staging'
  command: 'hugo server --bind=0.0.0.0 --appendPort=false --baseURL=staging.tylerruppert.com'
  environment:
    - 'ENV HUGO_BASEURL=http://staging.tylerruppert.com/'
  ports:
    - '80:1313'
```

#### Docker in Docker
In order to build and push the image to Docker, I needed to have Docker running.  There are a couple of choices here.  As a first go at this, I decided to bring Docker up using the official docker:1.12-dind image.  This has to run as privileged, as per the Dockerhub description.  Running a container as privileged gives it full access to the host environment.

#### Jenkins base setup:
I built Jenkins using Docker.  I decided to use the official jenkins:alpine image, as I don't need any custom config.  Here are the plugins I installed:
* Build Timeout Plugin
* Embedded Build Status Plugin
* Timestamper
* Workspace Cleanup Plugin
* Bitbucket Plugin
* Git Plugin
* GitHub Plugin
* Docker buildstep plugin

In order to use the Docker buildstep plugin, you need to set the Docker URL.  This can be found in Manage Jenkins >> Configure System >> Docker Builder.  We set it to "docker" because that is the name we gave our Docker in Docker service.  The "Test Connection" button gives an error, but that's appears to be a bug in the plugin.
```
Docker URL: http://docker:2375
```

#### Jenkins Project Setup:
I created a freestyle project for this.  The project does four things.

1. **Receive the webook from Bitbucket and trigger the build**

 In order to trigger the build, we need to select the box for "Build when a change is pushed to BitBucket" under the Build Triggers

2. **Download the code from Bitbucket**

 Under Source Code Management, set up the repository.  I set the Branches to build to "*/development" because I am going to be redeploying on a staging environment before merging into master.

3. **(Build Step) Build the docker image**

 Create a Build Step: Execute Docker command
 ```
 Docker command: Create/build image
 Build context folder: $WORKSPACE
 Tag of resulting docker image: ruppdog/homepage:staging
 ```

4. **(Build Step) Push the docker image to Dockerhub**

 Create a Build Step: Execute Docker command
 ```
 Docker command: Push image
 Name of the image to push (repository/image): ruppdog/homepage
 Tag: staging
 Docker registry URL: https://index.docker.io/v1/
 Registry credentials: Add your registry credentials.  They are required in order to push to the registry
 ```

#### That's all folks
Now each time I push to the development branch in Bitbucket, this process triggers.  As a final, not yet automated step, I log into Docker Cloud and redeploy the service that uses this image.  That brings it up to date with the current code.