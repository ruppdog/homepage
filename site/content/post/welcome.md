+++
date = "2016-08-08"
title = "Welcome"
description = "Welcome to my site!"
slug = "post"
+++

### What is this site?

I am building this website to showcase things I am interested in professionally.  I want to share my thoughts and ideas with anyone interested as I learn new things.  It will have original content written by yours truely, but I will also share articles that I read and find particularly interesting.

I'm also building this website as a very simple example piece of infrastructure that I want to keep running and highly available.  I will be building all the tooling around it as a proof of concept for many of the ideas in the continuous integration and deployment space.  I'll explain these ideas an the tools I used to implement them as they are added.

### What will I find here?

I plan on posting two types of content.

First, I will be posting original content related to DevOps and the tools I use and the things I learn.  An example of this is the continuous integration and deployment writeup that I am incrementally building as I set it up for this site.

Second, I will be posting links and summaries to articles that I find particularly useful.  I will only post articles that I have read and that I believe are useful.