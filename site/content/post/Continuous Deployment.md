+++
date = "2016-10-04"
title = "Continuous Deployment"
description = "Continuous Deployment pipeline I created for this site"
slug = "post"
+++

This is a continuation of my [previous post](/post/continuous-integration-example/) on Continuous Integration.  As I stated in my previous post, Continuous Integration is the act of continuously merging code as it is ready.  This is different from Continuous Deployment in that the code doesn't automatically go out to production.

In this post, I explain the Continuous Deployment pipeline that I created for this site.  This uses a lot of the setup I had from the Continuous Integration example, but goes further and automatically redeploys my app in a staging environment.

#### Tools Used
* [Git](https://git-scm.com) - I hope everyone uses some sort of source control to manage and version their code.  My source control of choice is Git.
* [Bitbucket](https://bitbucket.org) - Bitbucket works great for hosting my git repos.  [Github](https://github.com) would work just as well.  Both have the webhooks feature that we need for our build triggers
* [Kafka](http://kafka.apache.org) & [Zookeeper](https://zookeeper.apache.org) - Using Kafka was a main design choice.  I could have gone one of many other routes here, but I decided to use Kafka simply because I've been looking for a reason to try it out.  We could go a different route and use Amazon SNS for this.  It may be simpler, but it also ties you to to Amazon, as there isn't a mirror open source alternative.
* [Jenkins](https://jenkins.io) - This is a great first step for build triggers.  I use Jenkins because it is what I am familiar with.
* [Docker](https://www.docker.com) - Docker is greate both for long running services and short-lived tools.
* [sarama](https://github.com/truppert/sarama) - This is an open source Kafka library provided by Shopify.  I use my own fork of this because I added the ability to run it in a Docker container.
* [Pegasus](https://github.com/truppert/pegasus) - Pegasus is a tool I built in Golang that listens to my Kafka Topics and acts on them.

![Workflow](https://s3.amazonaws.com/tylerruppert-homepage-assets/Continuous-Deployment.png)

#### (1) Pushing Code
I'm using [git-flow](http://nvie.com/posts/a-successful-git-branching-model) for my git workflow.  Once I have a commit ready to go, I merge it into the development branch and push it to bitbucket.

#### (2) Building the app
In my previous post, I explain how to set up a webhook that automatically triggers a build in Jenkins.  I have changed this a bit from my previous post though.  While I'm still using the docker plugin, I decided to install docker on the Jenkins container.  This, coupled with a volume to the host to actually use the host node's docker daemon, allows me to run docker side-by-side with Jenkins instead of running a docker-in-docker.  It has some benefits, but mostly is just a simpler setup.

Here is what my Stackfile looks like in Docker Cloud for Jenkins now:
```
jenkins:
  image: 'ruppdog/jenkins:latest'
  ports:
    - '8080:8080'
  volumes:
    - /var/jenkins_home
    - '/var/run/docker.sock:/var/run/docker.sock'
```

On every build, Jenkins still builds the docker image and pushed it to dockerhub with the "staging" tag.

#### (3) Auto-Deploying to Staging
Once the staging image is built and pushed, it's time to deploy it to staging.  This is done using a messaging queue.  Here, I'm using Kafka, but any queue could be subbed in fairly simply.

As a step in the Jenkins build, I am pulling down the ruppdog/kafka-sarama image.  This is a fork of a golang kafka library that I modified to include Docker.  I did this so that I can execute a command using this container, essentially treating it as a service in order to publish to Kafka.  The command I run is as follows:
```
docker run \
--rm \
--link kafka-1.Core.1b6ea601:kafka \
ruppdog/kafka-sarama \
kafka-console-producer \
-topic staging-homepage \
-brokers kafka:9092 \
-key build-complete \
-value 36d74945-340d-4a6b-b9d2-46c760e05930
```

You can see that I used a link to a container instead of just referencing the kafka service by service name.  This is one of the gotchas currently.  On the current version of docker, Alpine Linux can't access the service using the service name.  This is an [bug](https://forums.docker.com/t/resolved-service-name-resolution-broken-on-alpine-and-docker-1-11-1-cs1/19307/8) that has yet to be squashed by the folks over at Docker.  Instead, I have to link to the full name of the Kafka container, then connect using the link.  I have to do the same thing for kafka talking to Zookeeper.

I am using the topic staging-homepage, connecting to the kafka link as my broker.  Messages in Kafka can have a key and a value, which works great for this.  Any listeners can act differently if they receive one key or another related to the "staging-homepage" topic.  I am using "build-complete" as the key, and the long uuid of the container that will need redeployed as the value.

On the other end of the Kafka queue, a golang app I built called Pegasus is subscibed to the "staging-homepage" topic.  When a message comes in, it knows to redeploy using the value when it sees a "build-complete" key.  That is currently the only key used.  Pegasus uses a docker-cloud library to trigger a redeploy of the service using the uuid.  Docker-cloud will automatically pull the most up to date version of the image it is using, which means that our app is now up to date.

The stack file for these pieces is as follows:
```
kafka:
  image: 'ruppdog/kafka:latest'
  environment:
    - ZOOKEEPER_HOST=zookeeper-1.Core.1eee9199
  expose:
    - '9092'
stagingprocessor:
  image: 'ruppdog/homepage-pegasus:latest'
  command: 'homepage-pegasus -brokers kafka:9092 -topic staging-homepage -dockercloud_user ruppdog -dockercloud_apikey [REMOVED]'
zookeeper:
  image: 'ruppdog/zookeeper:latest'
  expose:
    - '2181'
  ports:
    - '12181:2181'
  volumes:
    - /tmp/zookeeper

```

#### Success!
With this setup, every time I push to the develop branch, it automatically builds the app and releases it to staging.  No touch is needed from my end beyond pushing the code.  This is a great setup for staging, but often it is a good idea to have a push-button deploy for production.

#### Next Steps
The next steps for this is to extend the pipeline to include running tests.  If tests succeed, then the deploy can continue, but if they fail, it should stop immediately and alert me.

It could also be useful to build logging into this to track how often a build runs, succeeds, fails, etc.  It may also be useful to send alerts every time a build is executed.  This is all fairly simple using the Kafka queue we have now, and will be a great topic for a future post.