+++
title = "About Me"
+++

### Who am I?
My name is Tyler Ruppert.  I'm a DevOps Engineer currently working at SearchSpring.

I love learning everything I can about continuous integration and deployment, and every new tool that makes this easier to do safely.  I love working with bleeding edge tech!  Right now, my favorite tool to use is Docker, and I'm excitedly trying out all the new features as much as possible.